<?php
require('vendor/autoload.php');

use System\Core\App;

$app = new App();
$app->run();