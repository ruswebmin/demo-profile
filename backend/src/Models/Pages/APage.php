<?php

namespace Models\Pages;

/**
 * Абстрактный класс модели веб-страницы
 * @package Models\Pages
 */
abstract class APage
{
	/**
	 * @var string Наименование страницы
	 */
	protected string $title;

	/**
	 * @var string Наименование сущности
	 */
	protected string $entityName;

	/**
	 * @var array Обрабатываемые поля
	 */
	protected array $fields;

	/**
	 * @var array Исходящие данные
	 */
	protected array $outputData;

	/**
	 * Получить данные для html-представления
	 *
	 * @return array
	 */
	abstract public function getResult(): array;

	/**
	 * Получить промежуточные данные для html-представления
	 *
	 * @return array
	 */
	public function getPageData(): array {}

	/**
	 * Получить html-представление формы
	 *
	 * @param string $entityName
	 * @param array $fields
	 * @return string
	 */
	public function getForm(string $entityName, array $fields): string {}
}