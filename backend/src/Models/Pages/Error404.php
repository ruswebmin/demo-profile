<?php

namespace Models\Pages;

/**
 * Модель 404-ой страницы
 * @package Models\Pages
 */
class Error404 extends APage
{
	/**
	 * Error404 constructor
	 */
	public function __construct()
	{
		$this->title = 'LANG_TITLE_ERROR404';
	}

	/**
	 * @see APage::getResult()
	 * @return array
	 */
	public function getResult(): array
	{
		$this->outputData['title'] = $this->title;
		$this->outputData['message'] = 'LANG_ERR_PAGE_NOT_FOUND';
		$this->outputData['link'] = ['auth' => 'LANG_LINK_HOME'];

		return $this->outputData;
	}
}