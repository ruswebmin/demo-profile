<?php

namespace Models\Handlers;

use System\Helpers\Env,
	System\Helpers\Form\FormDataValidator,
	Delight\Auth as DelightAuth;

/**
 * Обработчик для авторизации зарегистрированных пользователей
 *
 * @package Models\Handlers
 */
class Login extends AHandler
{
	/**
	 * Login constructor
	 *
	 * @param string $entityName
	 * @param array $inputData
	 */
	public function __construct(string $entityName, array $inputData)
	{
		$this->entityName = $entityName;
		$this->inputData = $inputData;

		$validator = new FormDataValidator($this->entityName, $this->inputData);
		$errors = $validator->getErrors();
		if (!empty($errors)) {
			$this->outputData['errors'] = implode('<br/>', $errors);
		}
	}

	/**
	 * @see AHandler::getResult()
	 * @return string
	 */
	public function getResult(): string
	{
		if (!empty($this->outputData['errors'])) {
			return $this->outputData['result'] = $this->outputData['errors'];
		}

		if ($this->entityName === 'users' &&
			(isset($this->inputData['email']) && isset($this->inputData['password'])) &&
			(!empty($this->inputData['email']) && !empty($this->inputData['password']))
		) {
			$auth = Env::getSystemVar('auth');

			try {
				$auth->login($this->inputData['email'], $this->inputData['password']);
			} catch (DelightAuth\InvalidEmailException $e) {
				return 'LANG_ERR_WRONG_EMAIL';
			} catch (DelightAuth\InvalidPasswordException $e) {
				return 'LANG_ERR_WRONG_PASS';
			} catch (DelightAuth\EmailNotVerifiedException $e) {
				return 'LANG_ERR_EMAIL_VERIFY';
			} catch (DelightAuth\TooManyRequestsException $e) {
				return 'LANG_ERR_MANY_REQUESTS';
			}

			if ($auth->isLogin()) {
				$this->outputData['result'] = 'LANG_SYS_AUTH_SUCCESS!';
			} else {
				$this->outputData['result'] = 'LANG_SYS_AUTH_ERROR';
			}
		}

		return $this->outputData['result'];
	}
}