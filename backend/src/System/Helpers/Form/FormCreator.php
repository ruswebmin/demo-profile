<?php

namespace System\Helpers\Form;

use System\Helpers\Entity\Entity;

/**
 * Генератор форм
 * @package System\Helpers\Form
 */
class FormCreator
{
	/**
	 * @var Entity Сущность
	 */
	private Entity $entity;

	/**
	 * @var array Список доступных полей
	 */
	private array $fieldsAvailable;

	/**
	 * @var array|string[][] Фильтр учавствующих полей (byName - поимени, byType - по типу)
	 */
	private array $filterFields = [
		'byName' => [
			'id',
		],
		'byType' => [
			'timestamp',
		]
	];

	/**
	 * FormCreator constructor
	 * @param Entity $entity
	 * @param array $fieldsAvailable
	 */
	public function __construct(Entity $entity, array $fieldsAvailable = [])
	{
		$this->entity = $entity;
		$this->fieldsAvailable = $fieldsAvailable;
	}

	/**
	 * Сборка и получение html-формы
	 *
	 * @param string|null $handler
	 * @return string
	 */
	public function getView(string $handler = null): string
	{
		$fields = $this->entity->getFields();

		$inputsView = '';
		foreach ($fields as $field) {
			if (!empty($this->fieldsAvailable) && !in_array($field['Field'], $this->fieldsAvailable)) {
				continue;
			}

			if ($this->filterForView($field)) {
				$inputsView .= $this->getInputView($field) . PHP_EOL;
			}
		}

		return $this->getFormView($this->entity->getName(), $inputsView, $handler);
	}

	/**
	 * Фильтрация полей
	 *
	 * @param $field
	 * @return bool
	 */
	private function filterForView($field): bool
	{
		return !(in_array($field['Field'], $this->filterFields['byName']) ||
			in_array($field['Type'], $this->filterFields['byType']));
	}

	/**
	 * Получить html-представление поля ввода <input>
	 *
	 * @param array $field
	 * @return string
	 */
	private function getInputView(array $field): string
	{
		switch ($field['Field']) {
			case 'password' :
				$type = 'password';
				break;
			case 'photo' :
				$type = 'file';
				break;
			default:
				$type = 'text';
		}

		switch ($field['Field']) {
			case 'email' :
				$format = 'email';
				break;
			case 'phone' :
				$format = 'phone';
				break;
			default:
				$format = '';
		}

		switch ($field['Field']) {
			case 'photo' :
				$description = 'LANG_FILE_FIELD_LIMIT: ' .
					strtoupper(implode(', ', $field['extension'])) . ', ' .
					$field['length'] / 1024 . 'Kb';
				break;
			default:
				$description = '';
		}

		$required = (strtolower($field['Null']) == 'no') ? 'required' : '';

		return "<label><span>" . 'LANG_FIELD_' . strtoupper($field['Field']) . "</span>" .
			"<input name=\"$field[Field]\" type=\"$type\" $required " .
			"data-format=\"$format\" data-length=\"$field[length]\">" .
			"<div class=\"description\">$description</div>" .
			"<div class=\"info\"></div>" .
			"</label>";
	}

	/**
	 * Получить html-представление формы
	 *
	 * @param string $formName
	 * @param string $inputs
	 * @param string $mode
	 * @return string
	 */
	private function getFormView(string $formName, string $inputs, string $mode = ''): string
	{
		return "<form action=\"/$formName/$mode\" enctype=\"multipart/form-data\" method=\"post\">" . PHP_EOL .
			$inputs . PHP_EOL .
			"<input class=\"send-form-ajax\" type=\"submit\" value=\"LANG_BUTTON_SEND\">" . PHP_EOL .
			"</form>";
	}
}