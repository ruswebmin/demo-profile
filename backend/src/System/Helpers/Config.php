<?php

namespace System\Helpers;

/**
 * Конфигурация приложения
 * @package System\Helpers
 */
class Config
{
	/**
	 * @var string Файл конфигурации
	 */
	private string $configFile;

	/**
	 * Config constructor
	 *
	 * @param string $dir
	 */
	public function __construct(string $dir = '')
	{
		$this->configFile = Env::getConfigFile($dir);
	}

	/**
	 * Получить содержимое файла конфигурации
	 *
	 * @return array|false
	 */
	public function get(): array
	{
		return parse_ini_file($this->configFile);
	}
}