<?php

namespace System\Helpers\Entity;

/**
 * Составная структура сущностей
 * @package System\Helpers\Entity
 */
class EntityStructure
{
	/**
	 * @var string[][] Описание структур сущностей. Ключ - имя структуры, массив - список сущностей
	 */
	private static array $data = [
		'users' => [
			'users',
			'users_data'
		]
	];

	/**
	 * Получить структуру сущностей
	 *
	 * @param Entity $entity
	 * @return array
	 * @throws \Exception
	 */
	public static function get(Entity $entity): array
	{
		$entityStructure = [];
		if (!in_array($entity->getName(), array_keys(self::$data))) {
			$entityStructure[] = $entity;
		} else {
			foreach (self::$data[$entity->getName()] as $entityName) {
				if ($entityName === $entity->getName()) {
					$entityStructure[] = $entity;
				} else {
					$entityStructure[] = new Entity($entityName);
				}
			}
		}

		return $entityStructure;
	}

}