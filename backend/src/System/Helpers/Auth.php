<?php

namespace System\Helpers;

use Delight\Auth as DelightAuth;

/**
 * Аутентификация зарегистрированных пользователей
 * @package System\Helpers
 */
class Auth
{
	/**
	 * @var DelightAuth\Auth Объект авторизации
	 */
	private DelightAuth\Auth $auth;

	/**
	 * Auth constructor
	 */
	public function __construct()
	{
		$this->auth = new DelightAuth\Auth(Env::getSystemVar('dbConn'));
		Env::setSystemVar('auth', $this);
	}

	/**
	 * Прохождение процедуры аутентификации
	 *
	 * @param $email
	 * @param $password
	 * @return bool
	 * @throws DelightAuth\AttemptCancelledException
	 * @throws DelightAuth\AuthError
	 * @throws DelightAuth\EmailNotVerifiedException
	 * @throws DelightAuth\InvalidEmailException
	 * @throws DelightAuth\InvalidPasswordException
	 * @throws DelightAuth\TooManyRequestsException
	 */
	public function login($email, $password): bool
	{
		if (!$this->isLogin()) {
			$this->auth->login($email, $password);
		}

		return $this->auth->isLoggedIn();
	}

	/**
	 * Процедура выхода пользователя
	 *
	 * @return bool
	 * @throws DelightAuth\AuthError
	 * @throws DelightAuth\NotLoggedInException
	 */
	public function logout()
	{
		try {
			$this->auth->logOutEverywhere();
			$this->auth->destroySession();

			return true;
		} catch (Auth\NotLoggedInException $e) {
			die('Not logged in');
		}
	}

	/**
	 * Проверка аутентификаци пользователя
	 *
	 * @return bool
	 */
	public function isLogin(): bool
	{
		return $this->auth->isLoggedIn();
	}

	/**
	 * Процедура регистрации пользователя
	 *
	 * @param $email
	 * @param $password
	 * @param null $username
	 * @return int ID зарегистрированного пользователя
	 * @throws DelightAuth\AuthError
	 * @throws DelightAuth\InvalidEmailException
	 * @throws DelightAuth\InvalidPasswordException
	 * @throws DelightAuth\TooManyRequestsException
	 * @throws DelightAuth\UserAlreadyExistsException
	 */
	public function register($email, $password, $username = null): int
	{
		return $this->auth->register($email, $password, $username);
	}

	/**
	 * Удаление пользователя
	 *
	 * @param $userId
	 * @throws DelightAuth\AuthError
	 * @throws DelightAuth\UnknownIdException
	 */
	public function deleteUser($userId)
	{
		return $this->auth->admin()->deleteUserById($userId);
	}

	/**
	 * Получить e-mail пользователя
	 *
	 * @return string|null
	 */
	public function getEmail(): ?string
	{
		return $this->auth->getEmail();
	}
}