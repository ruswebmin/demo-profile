<?php

namespace System\Helpers;

/**
 * Языковые операции
 * @package System\Helpers
 */
class Lang
{
	/**
	 * @var array|false Список языковых констант с переводом
	 */
	private array $langList;

	/**
	 * @var string Символьный код выбранного языка
	 */
	private string $langSelected;

	/**
	 * Lang constructor
	 */
	public function __construct()
	{
		$this->langSelected = substr(
			filter_input(INPUT_COOKIE, 'lang', FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			0,
			2
		);

		if (empty($this->langSelected)) {
			$this->langSelected = Env::getSystemVar('config')['default_lang'];
		}

		$this->langList = parse_ini_file(Env::getLangFile($this->langSelected));
	}

	/**
	 * Получить символьный код текущего языка
	 *
	 * @return string
	 */
	public function getSelectedLang(): string
	{
		return $this->langSelected;
	}

	/**
	 * Получить список языковых констант с переводом
	 *
	 * @return array
	 */
	public function getLangList(): array
	{
		return $this->langList;
	}

	/**
	 * Перевод структуры с контентом на текущий язык
	 *
	 * @param array $content
	 * @return array
	 */
	public function translate(array $content): array
	{
		foreach ($content as $name => &$contentBlock) {
			foreach ($contentBlock as &$contentItem) {
				foreach ($this->langList as $wordTpml => $wordTslt)
					$contentItem = str_replace($wordTpml, $wordTslt, $contentItem);
			}
		}

		return $content;
	}
}