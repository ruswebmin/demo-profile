<?php

namespace System\Helpers;

/**
 * Окружение
 * @package System\Helpers
 */
class Env
{
	/**
	 * @var array Системные переменные, доступные в рамках приложения
	 */
	private static array $systemVars;

	/**
	 * Получить путь к корню приложения
	 *
	 * @return string
	 */
	public static function getHomeDir(): string
	{
		return $_SERVER['DOCUMENT_ROOT'] . '/';
	}

	/**
	 * Получить путь к директории с системными библиотеками
	 *
	 * @return string
	 */
	public static function getSystemDir(): string
	{
		return self::getHomeDir() . 'src/System/';
	}

	/**
	 * Получить путь к файлу конфигурации
	 *
	 * @param string $dir
	 * @return string
	 */
	public static function getConfigFile(string $dir = ''): string
	{
		$configFilename = 'config.ini';

		return (!empty($dir)) ? $dir . $configFilename : self::getSystemDir() . $configFilename;
	}

	/**
	 * Получить путь к языковому файлу
	 *
	 * @param string $lang
	 * @return string
	 */
	public static function getLangFile(string $lang): string
	{
		return self::getSystemDir() . "Langs/$lang.ini";
	}

	/**
	 * Назначить системную переменную
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public static function setSystemVar(string $key, $value)
	{
		self::$systemVars[$key] = $value;
	}

	/**
	 * Получить системныу переменную
	 *
	 * @param string $key
	 * @return mixed|null
	 */
	public static function getSystemVar(string $key)
	{
		return !empty($key) ? self::$systemVars[$key] : null;
	}
}