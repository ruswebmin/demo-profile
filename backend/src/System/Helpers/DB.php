<?php

namespace System\Helpers;

/**
 * Работа с БД
 * @package System\Helpers
 */
class DB
{
	/**
	 * @var \PDO Объект подключения к БД
	 */
	private \PDO $pdo;

	public function __construct() {}

	/**
	 * Подключение к БД
	 *
	 * @return \PDO
	 */
	public function connect(): \PDO
	{
		$config = Env::getSystemVar('config');

		$dsn = $config['db_driver'] . ':host=' . $config['host'];
		$username = $config['db_user'];
		$password = $config['db_password'];

		try {
			$this->pdo = new \PDO($dsn, $username, $password);
		} catch (\Exception $e) {
			die('Error: ' . $e->getMessage());
		}

		if ($this->pdo->query("USE " . $config['db_name']) === false) {
			$dbCreateRes = $this->pdo->query("CREATE DATABASE " . $config['db_name']);

			if (!$dbCreateRes) {
				die('DB Error: ' . $this->pdo->errorInfo()[2]);
			} else {
				$this->pdo->query("USE " . $config['db_name']);
				$dump = file_get_contents(Env::getHomeDir() . 'db/' . $config['db_name'] . '.sql');
				$this->pdo->query($dump);
			}
		}

		return $this->pdo;
	}

	/**
	 * Отключение от БД
	 */
	public function disconnect()
	{
		unset($this->pdo);
	}
}