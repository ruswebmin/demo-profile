<?php 

class UserCest
{
	private $email;
	private $password;

	public function __construct()
	{
		$this->email = 'test' . time() . '@test.ru';
		$this->password = 'testpass000#';
	}

	public function tryUserRegistration(AcceptanceTester $I)
	{
		$I->amOnPage('/reg');
		$I->submitForm('.send-form-ajax',
			[
				'email' => $this->email,
				'password' => $this->password
			]);
		$I->see('User successfully registered');
	}

    public function tryLoginAndReadProfile(AcceptanceTester $I)
    {
    	// Login
		$I->amOnPage('/auth');
		$I->submitForm('.send-form-ajax', [
			'email' => $this->email,
			'password' => $this->password
		]);
		$I->see('Login successful');

		// Read profile
		$I->amOnPage('/profile');
		$I->seeInTitle('Profile');
		$I->seeElement('ul');
		$I->see($this->email);
    }
}
