# Profile demo #

Веб-приложение с демонстрацией функций регистрации, аутентификации и хранении данных пользователя.

## Реализация ##

Приложение подготовлено на основе MVC-архитектуры и состоит из серверной (папка backend) и клиентской (папка frontend) частей.
В функциональный набор приложения входит:
- регистрация пользователя;
- аутентификация пользователя;
- хранение текстовых и графических данных в БД, предоставленных пользователем;
- верификация входящих данных как на клиентской так и на серверной стороне;
- языковая поддержка интерфейса (русский, английский) с возможностью переключения языков;
- адаптивная верстка, основанная на Bootstrap 4.

В папке backend содержатся следующие компоненты:
- db: дамп базы данных (разворачивается автоматически);
- src: классы серверной части приложения;
- tests: демонстрационные набор для тестирования посредством Codeception с частичным покрытием кода.

В папке frontend содержатся компоненты верстки, в т.ч. html-шаблоны (подготовленные для Twig-шаблонизатора), стили в Scss-формате, обслуживающие JQuery-скрипты. 
Для тестирования элементов верстки нужно воспользоваться gulp-сборщиком:
```
gulp htmlbuild
```
Сборка будет доступна в папке frontend/build

Посредством gulp-сборщика все изменения по части frontend будут перенесены в общий build и автоматически связаны с backend.
Для автоматической корректировки build во время внесения изменений достаточно запустить команду:
 ```
 gulp watch
 ```

## Технические требования ##

- php: 7.4
- mysql: 5.7


## Сборка ##

В папке build уже присутствует готовая сборка.
Базовые настройки, в т.ч. подключение к БД, можно поменять в файле конфигурации: build/src/System/config.ini
База данных будет автоматически развернута при первом запуске.

Подготовка к сборке и установка вендорных библиотек.
```
# frontend
yarn install

# backend
cd backend
composer update
```

Для новой сборки:
```
gulp build
```

Очистить временные папки для сборки:
```
gulp clean
```


## Тестирование ##

Скрипты для тестирования приложения, находятся в папке backend/tests и запускаются с помощью библиотеки Codeception.
```
cd backend
composer update
php vendor/bin/codecept run
```
В ознакомительных целях здесь представлены несколько функционально-приемочных и модульных тестов.

Для автоматического тестирования проекта в git-репозитории подготовлен сценарий для Travis CI (.travis.yml).

## Запуск ##
В папке docker-compose-lamp подготовлена docker-конфигурация для быстрого запуска и ознакомления с веб-приложением. 
```
cd docker-compose-lamp
docker-compose up -d
```