$(document).ready(function() {
    /**
     * Language controls
     */
    $('.lang div').on('click', function(event) {
        event.preventDefault();
        $.cookie('lang', $(this)[0].className);
        location.reload();
    });
});