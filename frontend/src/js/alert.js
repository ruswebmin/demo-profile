$(document).ready(function() {

});

/**
 * Alert show on page
 */
function alertShow(alertText = '', alertStatus = 'info') {
    $('.alert .close').click();
    alertStatus = 'alert-' + alertStatus;
    let content = '<div class="alert ' + alertStatus + ' alert-dismissible fade show" role="alert">\n' +
        '<div>' + alertText +'</div>' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
        '<span aria-hidden="true">&times;</span>\n' +
        '</button>\n' +
        "</div>";
    $('body').prepend(content);
}