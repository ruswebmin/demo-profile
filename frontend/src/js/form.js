$(document).ready(function() {
    /**
     * Ajax send data from forms to handlers
     */
    $('.send-form-ajax').on('click', function(event) {
        event.preventDefault();

        let form = $(this).closest('form');
        formValidate(form);

        let formData = new FormData($('form')[0]);
        let thisButton = $(this);
        let loader = $('.loader');

        if (form[0].method === 'post') {
            thisButton.prop('disabled', true);
            loader.show();
            $.ajax({
                url: form[0].action,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: form[0].method,
            })
                .done(function(msg) {
                    // symbol "!" is success of operation
                    if (msg.indexOf('!') >= 0) {
                        alertShow(msg, 'success');
                        window.location.replace('/');
                    } else {
                        alertShow(msg, 'warning');
                    }
                })
                .fail(function(jqXHR, textStatus) {
                    alertShow("Request failed: " + textStatus, 'danger');
                })
                .always(function() {
                    loader.hide();
                    thisButton.prop('disabled', false);
                });
        }
    });
});

/**
 * Form validator
 */
function formValidate(form) {
    let formFields = form.find('input[type=text],input[type=password]');

    formFields.each(function() {
        let required = $(this)[0].required;
        let strlen = ($(this).val()).length;
        $(this).siblings('.info').text('');
        if ($(this).hasClass('error')) {
            $(this).removeClass('error');
        }

        if ($(this).data('format') === 'email' ||
            ($(this).data('format') === 'phone' && strlen > 0))
        {
            if (validateFormat($(this).val(), $(this).data('format')) === false) {
                errorField($(this), 'ALERT_INCORRECT_FORMAT');
            }
        }

        if (required && strlen === 0) {
            errorField($(this), 'ALERT_REQUIRE_FIELD');
        }

        if (strlen > $(this).data('length')) {
            errorField($(this), 'ALERT_LIMIT_TEXT');
        }
    })
}

function validateFormat(value, format) {
    let re;
    switch (format) {
        case 'email':
            re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            break;
        case 'phone':
            re = /^[0-9]{5,12}$/;
            break;
    }

    return re.test(String(value).toLowerCase());
}

function errorField(target, text) {
    target.addClass('error');
    target.siblings('.info').
        text($('.alert-messages').find('.' + text).text());
}