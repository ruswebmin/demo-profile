<?php

use System\Helpers;

class EntityTest extends \Codeception\Test\Unit
{
    public function __construct(?string $name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);

		$config = new Helpers\Config(__DIR__ . '/../../src/System/');
		Helpers\Env::setSystemVar('config', $config->get());

		$db = new Helpers\DB();
		$dbConn = $db->connect();
		Helpers\Env::setSystemVar('dbConn', $dbConn);
	}

	public function testHelperEntity()
    {
		$entity = new Helpers\Entity\Entity('users_data');
		$entityName = $entity->getName();
		$entityFields = $entity->getFields();
		$entityData = $entity->getData();

		$this->assertEquals('users_data', $entityName);
		$this->assertEquals(['id', 'user_id', 'phone', 'country', 'town', 'address', 'photo'],
			array_keys($entityFields));
		$this->assertEquals(['id', 'user_id', 'phone', 'country', 'town', 'address', 'photo'],
			array_keys($entityData[array_key_first($entityData)]));
    }

	public function testHelperEntityStructure()
	{
		$entity = new Helpers\Entity\Entity('users');
		$entityStructure = Helpers\Entity\EntityStructure::get($entity);

		$this->assertEquals(2, count($entityStructure));
	}
}