<?php

use System\Helpers,
	Models\Pages;

class PageTest extends \Codeception\Test\Unit
{
	public function __construct(?string $name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);

		$config = new Helpers\Config(__DIR__ . '/../../src/System/');
		Helpers\Env::setSystemVar('config', $config->get());

		$db = new Helpers\DB();
		$dbConn = $db->connect();
		Helpers\Env::setSystemVar('dbConn', $dbConn);
	}

    public function testPageAuth()
    {
		$page = new Pages\Auth();
		$pageResult = $page->getResult();

		$this->assertEquals('LANG_TITLE_AUTH', $pageResult['title']);
		$this->assertEquals('LANG_FIELD_EMAILLANG_FIELD_PASSWORD',
			strip_tags(str_replace("\n", '', $pageResult['form'])));
    }
}