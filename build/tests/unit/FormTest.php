<?php

use System\Helpers;

class FormTest extends \Codeception\Test\Unit
{
	public function __construct(?string $name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);

		$config = new Helpers\Config(__DIR__ . '/../../src/System/');
		Helpers\Env::setSystemVar('config', $config->get());

		$db = new Helpers\DB();
		$dbConn = $db->connect();
		Helpers\Env::setSystemVar('dbConn', $dbConn);
	}

    public function testHelperFormCreator()
    {
		$entity = new Helpers\Entity\Entity('users');
		$formCreator = new Helpers\Form\FormCreator($entity, ['username', 'password']);

		$this->assertEquals('LANG_FIELD_PASSWORDLANG_FIELD_USERNAME',
			strip_tags(str_replace("\n", '', $formCreator->getView('login'))));
    }

    public function testHelperFormValidator()
    {
		$formValidator = new Helpers\Form\FormDataValidator('users', ['username', 'password']);

		$this->assertEquals(
			[
				0 => 'LANG_ERR_VALUE_NONE: LANG_FIELD_EMAIL',
				1 => 'LANG_ERR_VALUE_NONE: LANG_FIELD_PASSWORD'
			],
			$formValidator->getErrors()
		);
    }
}