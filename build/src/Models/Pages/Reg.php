<?php


namespace Models\Pages;

use System\Helpers\Entity\Entity,
	System\Helpers\Form\FormCreator,
	System\Helpers\Lang;

/**
 * Модель страницы регистрации нового пользователя
 * @package Models\Pages
 */
class Reg extends APage
{
	/**
	 * Reg constructor
	 */
	public function __construct()
	{
		$this->title = 'LANG_TITLE_REG';
		$this->fields = [
			'email',
			'username',
			'password',
			'phone',
			'country',
			'town',
			'address',
			'photo',
		];
	}

	/**
	 * @see APage::getResult()
	 * @return array
	 * @throws \Exception
	 */
	public function getResult(): array
	{
		$this->outputData['title'] = $this->title;
		$formUser = $this->getForm('users', ['email', 'username', 'password']);
		$formUserData = $this->getForm('users_data');

		$this->outputData['form'] = preg_replace(
			'/<input.+submit.+\n.+\"post\">/m',
			'',
			$formUser . $formUserData
		);

		$langList = (new Lang())->getLangList();
		$alertMsg = '';
		foreach ($langList as $tmpl => $trnsl) {
			if (strpos($tmpl, 'LANG_ALERT') !== false) {
				$tmpl = str_replace('LANG_', '', $tmpl);
				$alertMsg .= "<div class=\"$tmpl\">$trnsl</div>";
			}
		}

		$this->outputData['link'] = ['auth' => 'LANG_LINK_AUTH'];
		$this->outputData['alerts'] = $alertMsg;

		return $this->outputData;
	}

	/**
	 * @see APage::getForm()
	 * @param string $entityName
	 * @param array $fields
	 * @return string
	 * @throws \Exception
	 */
	public function getForm(string $entityName, array $fields = []): string
	{
		$entity = new Entity($entityName);
		$form = new FormCreator($entity, $this->fields);

		return $form->getView('registration');
	}
}