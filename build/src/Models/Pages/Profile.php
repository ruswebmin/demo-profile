<?php

namespace Models\Pages;

use System\Helpers\Entity\Entity,
	System\Helpers\Env;

/**
 * Модель страницы профиля
 * @package Models\Pages
 */
class Profile extends APage
{
	/**
	 * Profile constructor
	 */
	public function __construct()
	{
		$this->title = 'LANG_TITLE_PROFILE';
		$this->fields = [
			'email',
			'username',
			'phone',
			'country',
			'town',
			'address',
			'photo',
		];
	}

	/**
	 * @see APage::getResult()
	 * @return array
	 */
	public function getResult(): array
	{
		$this->outputData['title'] = $this->title;
		$this->outputData['data'] = $this->getPageData();
		$this->outputData['link'] = ['logout' => 'LANG_LINK_LOGOUT'];

		return $this->outputData;
	}

	/**
	 * @see APage::getPageData()
	 * @return array
	 */
	public function getPageData(): array
	{
		$auth = Env::getSystemVar('auth');

		$entityUsers = new Entity('users');
		$entityUsersData = new Entity('users_data');
		$userEmail = $auth->getEmail();

		$userId = null;
		$userDataPrimary = [];
		foreach ($entityUsers->getData() as $field => $userData) {
			if ($userData['email'] === $userEmail) {
				$userDataPrimary = $userData;
			}
		}

		$userDataSecondary = [];
		if (!empty($userDataPrimary)) {
			foreach ($entityUsersData->getData() as $userData) {
				if ($userData['user_id'] === $userDataPrimary['id']) {
					$userDataSecondary = $userData;
				}
			}
		}

		// Изображение аватара пользователя по-умолчанию
		if (strlen($userDataSecondary['photo']) < 32) {
			$userDataSecondary['photo'] = '/9j/4AAQSkZJRgABAQEAYABgAAD/4QBoRXhpZgAATU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAAExAAIAAAARAAAATgAAAAAAAABgAAAAAQAAAGAAAAABcGFpbnQubmV0IDQuMi4xMAAA/9sAQwAQCwwODAoQDg0OEhEQExgoGhgWFhgxIyUdKDozPTw5Mzg3QEhcTkBEV0U3OFBtUVdfYmdoZz5NcXlwZHhcZWdj/9sAQwEREhIYFRgvGhovY0I4QmNjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2Nj/8AAEQgAewCAAwEhAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A9AooAKKAKl5qVrZD99IN3ZByTWHdeJpWJFtCqD+8/JppCbMybVb6Y/PdSfRTtH6VXaaVj80jn6saokFmlU5WRx9GNWYdWv4T8lzIfZju/nQBp2vieRSBdQhh/eTg/lW5Z6hbXq5glBPdTwR+FS0UmWqKQwooAKKAEZgqlmIAHJJ7Vzeq+ISS0NicDoZfX6U0hNnPszOxZiWY9Se9JVkhRQAUUAFKjtG4ZGKsOhBwRQB0WleIclYb4+wl/wAa6IEEAg5B71DRSYtFIYUhIAyTgCgDktc1hrtzb27YgU8kfxn/AArGq0QwopgFFABRQAUUAFbehaybZ1trlswnhWP8H/1qTGjq6WoKCuf8TakYk+xxNhnGZCOw9KaEzl6KskKKACpYraeYZihkcf7Kk0AEtvNB/rYnT/eUioqACigAooA6vw3qRuIfssrZkjHyk91/+tW7UMpEVxMtvbyTP91FJNcBczvc3Ek0hyznJpoTI6KoQlFAG54e0lLxjcXAzEhwq/3j/hXVqqooVVCqOgAqGUgkjSRCkihlPUEZFcfr+lrYTLLCP3Mh6f3T6UIGZFFWSFFICazuXtLqOdOqHP1HpXoEUizRJIhyrgEUmUjG8VXHlWCQg8ytz9B/kVyVCEwopiCigDudCQJo9uB3XP5mtCoLCsrxIgbR5SeqlSPzoQM4uirICigArsPDFx52meWTkxMV/DqKTGjL8Wy7r+KPPCR5/En/AOtWFTQMKKBBRQB2nhqcTaSi5+aIlT/MfzrWqGWFYniqcR6asWfmlccew5/wpoTOQoqiQooAK6DwhLi5uIv7yBvyP/16GNFXxOc6w3si1kUIGFFAgooA1NB1IafeYkP7mThvb0NdsCGAIOQehqWUhHdY0Z3IVVGST2FcLrOoHUL0uufKX5UHt60IGUKKokKKACtrwof+Jqw9Yj/MUMaE8Vpt1UN/ejB/mKxaEDCigQUUAaFho15fYZI9kZ/jfgfh613EEYhgjiByEULn1wKllIZewG6spoA20yKQD6Vw97pV5Y8zRHZ/fXkU0DKVFMkKKACt3wim7UZX7LEf1IoY0WvGEHy29wB0yh/mP61zFJAwopiAcnArqtF8PrGq3F8u5zysZ6L9fekxo6EDAwKWpKCkIDAggEHsaAOd1rw8rq1xYrtcctEOh+lcseDg1SJYlFMQV1fhCDbbzzkffYKPw/8A10mNGprNp9t0yaIDL43L9RXAdKEDCimI3fC1gtxdNcyDKQ/dB7t/9auwqWUgopDCigArjvFNitteLcRjCTZyPRu9NCZh0VRIAFiABknpXoWl2v2LT4YP4lXLfU9aTGi3XE+JNONnfGVB+5mO4ex7ikhsx6Kok3NG12LTbMwtAzsXLEg4q/8A8JdD/wA+sn/fQpWHcP8AhLof+fWT/voUf8JdD/z6yf8AfQpWHcP+Euh/59ZP++hR/wAJdD/z6yf99CiwXD/hLof+fWT/AL6FZ2ta3FqdskSwMhV92Sc9qdhXMWimI2/DGnG6vPtEi/uoTn6t2rs6llIKrX9nHf2jwSjg9D/dPrSGcBe2ktjctBMuGXoexHqKgqyAooAKKACigAooAKs2FlLf3SwQjk9W7KPU0Ad/ZWkdlapBCMKo6+p9anqCwooAo6rpkOpwbJPlkX7jjqv/ANauHv7C40+cxTpj0YdG+lUhMq0UyQooAKKACigC3p+nXGozeXAvA+856LXc6Zp0Om2/lRDLHl3PVjUspFyikMKKACobm2hu4TFPGHQ9jQBzGo+FZUJexfzF/wCebHBH496wJ4Jrd9k8bxt6MMVSZLRHRTEJRQBLb209y+yCJ5G9FGa6HTvCrsRJfvtX/nmh5/E0mxpHTW9vFbRCKCNUQdAKlqSgooAKKACigApksUcybJY1dfRhkUAYeraPp8cJkS2VW/2SR+ma5CZQspCjAFUiWLAoeUKwyK7DStG09oRI9srN/tEn9M0MEbUcccSbY0VFHZRgU+pKCigAooA//9k=';
		}

		$userData = array_filter(
			array_merge($userDataPrimary, $userDataSecondary),
			function ($key) {
				return in_array($key, $this->fields);
			},
			ARRAY_FILTER_USE_KEY
		);

		array_walk(
			$userData,
			function(&$item, $key) {
				if ($key != 'photo') {
					$item = "LANG_FIELD_" . strtoupper($key) . ": " . $item;
				}
			}
		);

		return $userData;
	}
}