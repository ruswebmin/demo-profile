<?php

namespace Models\Pages;

use System\Helpers\Form\FormCreator,
	System\Helpers\Entity\Entity;

/**
 * Модель страницы авторизации пользователей
 * @package Models\Pages
 */
class Auth extends APage
{
	/**
	 * Auth constructor
	 */
	public function __construct()
	{
		$this->title = 'LANG_TITLE_AUTH';
		$this->entityName = 'users';
		$this->fields = [
			'email',
			'password'
		];
	}

	/**
	 * @see APage::getResult()
	 * @return array
	 * @throws \Exception
	 */
	public function getResult(): array
	{
		$this->outputData['title'] = $this->title;
		$this->outputData['form'] = $this->getForm($this->entityName, $this->fields);
		$this->outputData['link'] = ['reg' => 'LANG_LINK_REG'];

		return $this->outputData;
	}

	/**
	 * @see APage::getForm()
	 * @param string $entityName
	 * @param array $fields
	 * @return string
	 * @throws \Exception
	 */
	public function getForm(string $entityName, array $fields): string
	{
		$entity = new Entity($entityName);
		$form = new FormCreator($entity, $fields);

		return $form->getView('login');
	}
}