<?php

namespace Models\Handlers;

use Delight\Auth,
	System\Helpers\Env,
	System\Helpers\Form\FormDataValidator;

/**
 * Обработчик для регистрации новых пользователей
 * @package Models\Handlers
 */
class Registration extends AHandler
{
	/**
	 * Registration constructor
	 *
	 * @param string $entityName
	 * @param array $inputData
	 */
	public function __construct(string $entityName, array $inputData)
	{
		$this->entityName = $entityName;
		$this->inputData = $inputData;

		$validator = new FormDataValidator($this->entityName, $this->inputData);
		$errors = $validator->getErrors();
		if (!empty($errors)) {
			$this->outputData['errors'] = implode('<br/>', $errors);
		}
	}

	/**
	 * @see AHandler::getResult()
	 */
	public function getResult(): string
	{
		if (!empty($this->outputData['errors'])) {
			return $this->outputData['errors'];
		}

		$auth = Env::getSystemVar('auth');
		$dbConn = Env::getSystemVar('dbConn');

		try {
			$userId = $auth->register(
				$this->inputData['email'],
				$this->inputData['password'],
				$this->inputData['username']
			);
		} catch (Auth\InvalidEmailException $e) {
			return 'LANG_ERR_EMAIL_REQUIRE';
		} catch (Auth\InvalidPasswordException $e) {
			return 'LANG_ERR_PASS_REQUIRE';
		} catch (Auth\UserAlreadyExistsException $e) {
			return 'LANG_ERR_EMAIL_USER_EXISTS';
		} catch (Auth\DuplicateUsernameException $e) {
			return 'LANG_ERR_USERNAME_EXISTS';
		}

		try {
			$result = false;
			$errorMessage = '';

			if (!empty($this->inputData['photo']['name'])) {
				$this->inputData['photo'] = base64_encode(file_get_contents($this->inputData['photo']['tmp_name']));
			} else {
				unset($this->inputData['photo']);
			}

			if (!empty($userId) && $userId > 0) {
				$dataForEntity = array_filter(
					$this->inputData,
					function ($fieldName) {
						return !in_array($fieldName, ['email', 'password', 'username']);
					},
					ARRAY_FILTER_USE_KEY);

				$dataForEntity = array_merge($dataForEntity, ['user_id' => $userId]);

				$query = "INSERT INTO users_data (" . implode(",", array_keys($dataForEntity)) . ") " .
					"VALUES(:" . implode(", :", array_keys($dataForEntity)) . ")";

				$sth = $dbConn->prepare($query);
				$result = $sth->execute($dataForEntity);

				if (!$result) {
					$errorMessage = $sth->errorInfo()[2];
				}
			}
		} catch (\Exception $e) {
			echo $e->getMessage();
		}

		if (!$result && !empty($userId)) {
			$auth->deleteUser($userId);
		}

		$this->outputData['result'] = $result ?
			'LANG_SYS_USER_REG_SUCCESS!' :
			'LANG_SYS_USER_REG_ERROR: ' . $errorMessage;

		return $this->outputData['result'];
	}
}