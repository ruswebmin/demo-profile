<?php


namespace Models\Handlers;

/**
 * Class AHandler
 *
 * @package Models\Handlers
 */
abstract class AHandler
{
	/**
	 * @var string Наименование сущности
	 */
	protected string $entityName;

	/**
	 * @var array Набор входящих данных
	 */
	protected array $inputData;

	/**
	 * @var array Набор исходящих данных
	 */
	protected array $outputData;

	/**
	 * Получить результаты обработчика
	 *
	 * @return string
	 */
	abstract public function getResult(): string;
}