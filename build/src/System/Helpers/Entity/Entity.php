<?php

namespace System\Helpers\Entity;

use System\Helpers\Env;

/**
 * Сущность
 * @package System\Helpers\Entity
 */
class Entity
{
	/**
	 * @var \PDO|null Подключение к БД
	 */
	private ?\PDO $dbConn;

	/**
	 * @var string Наименование сущности из БД
	 */
	private string $entityName;

	public function __construct(string $name)
	{
		$this->dbConn = Env::getSystemVar('dbConn');
		$this->entityName = $name;

		if (!in_array($this->entityName, $this->getEntitiesList())) {
			throw new \Exception('Entity ' . $this->entityName . ' not found!');
		}
	}

	/**
	 * Получить наименование сущности
	 *
	 * @return string
	 */
	public function getName(): string
	{
		return $this->entityName;
	}

	/**
	 * Получить данные сущности
	 *
	 * @return array
	 */
	public function getData(): array
	{
		$data = [];
		$dbo = $this->dbConn->query('SELECT * FROM ' . $this->entityName, \PDO::FETCH_ASSOC);
		foreach ($dbo as $row) {
			isset($row['id']) ? $data[$row['id']] = $row : $data[] = $row;
		}

		return $data;
	}

	/**
	 * Получить список и параметры полей сущности
	 *
	 * @return array
	 */
	public function getFields(): array
	{
		$query = 'DESCRIBE ' . $this->entityName;
		$queryObj = $this->dbConn->prepare($query);
		$queryObj->execute();

		$fields = [];
		foreach ($queryObj->fetchAll(\PDO::FETCH_ASSOC) as $row) {
			$m = null;
			if (isset($row['Type'])) {
				preg_match('/\d+/', $row['Type'], $m);
			}
			$fieldLength = $m[0] ?? null;

			if (strtolower($row['Type']) === 'blob') {
				$fieldLength = 32768;
				$row = array_merge($row, [
						'extension' =>
							[
								'jpg'
							]
					]
				);
				$row = array_merge($row, [
						'mime' =>
							[
								'image/jpeg',
								'image/pjpeg',
							]
					]
				);
			}

			$fields[$row['Field']] = array_merge($row, ['length' => $fieldLength]);
		}

		return $fields;
	}

	/**
	 * Получить список сущностей
	 *
	 * @return array
	 */
	private function getEntitiesList(): array
	{
		return $this->dbConn->query('SHOW TABLES')->fetchAll(\PDO::FETCH_COLUMN);
	}
}