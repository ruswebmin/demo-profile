<?php

namespace System\Helpers\Form;

use System\Helpers\Entity\Entity;
use System\Helpers\Entity\EntityStructure;

/**
 * Валидатор входящих данных форм
 * @package System\Helpers\Form
 */
class FormDataValidator
{
	/**
	 * @var array Структура сущностей
	 */
	private array $entityStructure;

	/**
	 * @var array Входящие данные
	 */
	private array $inputData;

	/**
	 * @var string[] Поля для проверки
	 */
	private array $fieldsValid;

	/**
	 * FormDataValidator constructor
	 *
	 * @param string $entityName
	 * @param array $inputData
	 * @throws \Exception
	 */
	public function __construct(string $entityName, array $inputData)
	{
		$this->entityStructure = EntityStructure::get(new Entity($entityName));
		$this->inputData = $inputData;
		$this->fieldsValid = [
			'email',
			'username',
			'password',
			'phone',
			'country',
			'town',
			'address',
			'photo',
		];
	}

	/**
	 * Получить список ошибок при проверке входящих данных формы
	 *
	 * @return array
	 */
	public function getErrors(): array
	{
		$entityFields = [];
		foreach ($this->entityStructure as $entity) {
			$entityFields += $entity->getFields();
		}

		$entityFieldsActual = array_filter(
			$entityFields,
			function ($field) {
				return in_array($field, $this->fieldsValid);
			},
			ARRAY_FILTER_USE_KEY);

		$errors = [];
		foreach ($entityFieldsActual as $fieldName => $fieldParam) {

			// checking field value
			if (strtolower($fieldParam['Null']) === 'no' &&
				(!isset($this->inputData[$fieldParam['Field']]) || empty($this->inputData[$fieldParam['Field']]))) {
				$errors[] = 'LANG_ERR_VALUE_NONE: ' . 'LANG_FIELD_' . strtoupper($fieldParam['Field']);
			}

			// size and format checking
			if (isset($this->inputData[$fieldParam['Field']])) {

				// file validation
				if (
					(strtolower($fieldParam['Type']) === 'blob' &&
						!empty($this->inputData[$fieldParam['Field']]['name'])
					) &&
					(
						(
							$this->inputData[$fieldParam['Field']]['size'] > $fieldParam['length']
						) ||
						(
						!in_array(
							pathinfo($this->inputData[$fieldParam['Field']]['name'], PATHINFO_EXTENSION),
							$fieldParam['extension']
						)
						) ||
						(
						!in_array(
							$this->inputData[$fieldParam['Field']]['type'],
							$fieldParam['mime']
						)
						)
					)
				) {
					$errors[] = 'LANG_ERR_FILE_LIMIT: ' . 'LANG_FIELD_' . strtoupper($fieldParam['Field']);

					// validation other types
				} elseif (!is_array($this->inputData[$fieldParam['Field']]) &&
					strlen($this->inputData[$fieldParam['Field']]) > $fieldParam['length']) {
					$errors[] = 'LANG_ERR_VALUE_LIMIT: ' . 'LANG_FIELD_' . strtoupper($fieldParam['Field']);
				}
			}
		}

		return $errors;
	}
}