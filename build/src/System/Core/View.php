<?php

namespace System\Core;

use Twig\Loader\FilesystemLoader,
	Twig\Environment,
	Twig\Error;

/**
 * Представление в виде html-кода посредством шаблонизатора
 * @package System\Core
 */
class View
{
	/**
	 * @var array|mixed Данные для шалона
	 */
	private array $data;

	/**
	 * @var string Наименование шаблона
	 */
	private string $template;

	/**
	 * @var Environment Шаблонизатор
	 */
	private Environment $twig;

	/**
	 * View constructor
	 * @param $modelData
	 */
	public function __construct($modelData)
	{
		$this->template = (string)key($modelData);
		$this->data = $modelData[$this->template];

		$loader = new FilesystemLoader([
			'src/Views',
			'src/Views/components'
		]);
		$this->twig = new Environment($loader, []);
	}

	/**
	 * Подготовка html-кода по шаблону
	 *
	 * @throws Error\LoaderError
	 * @throws Error\RuntimeError
	 * @throws Error\SyntaxError
	 */
	public function render()
	{
		echo $this->twig->render($this->template . '.html', ['page' => $this->data]);
	}
}