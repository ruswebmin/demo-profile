<?php

namespace System\Core;

use Models\Pages,
	System\Helpers\Auth,
	System\Helpers\Entity\Entity,
	System\Helpers\Entity\EntityStructure,
	System\Helpers\Lang;

/**
 * Модель для подготовки данных
 * @package System\Core
 */
class Model
{
	/**
	 * @var Controller Контроллер
	 */
	private Controller $controller;

	/**
	 * @var string|null Наименование обрабатываемой страницы
	 */
	private ?string $page;

	/**
	 * @var string Действие, производимое с данными
	 */
	private string $action;

	/**
	 * @var string Наименования сущности в БД
	 */
	private string $entityName;

	/**
	 * @var array|array[] Получаемый результат подготовки данных
	 */
	private array $result;

	/**
	 * Model constructor
	 * @param Controller $controller
	 * @throws \Exception
	 */
	public function __construct(Controller $controller)
	{
		$this->controller = $controller;
		$auth = new Auth();
		$this->controller->setPageRedirect($auth);

		switch ($controller->getMethod()) {
			case 'GET':
				$this->page = $controller->getPage();
				$this->result = $this->processingPageData();
				break;

			case 'POST':
				$this->action = $controller->getAction();
				$this->entityName = $controller->getEntityName();
				$entity = new Entity($this->entityName);
				$this->result = $this->processingFormData($entity);
				break;

			default:
				throw new \Exception("Model hasn't data about request method");
		}
	}

	/**
	 * Получить готовые данные, в зависимости от установленного языка
	 *
	 * @param Lang $lang
	 * @return array
	 */
	public function getData(Lang $lang): array
	{
		return $lang->translate($this->result);
	}

	/**
	 * Обработка данных страницы
	 *
	 * @return array
	 * @throws \Exception
	 */
	private function processingPageData(): array
	{
		if (empty($this->page)) {
			throw new \Exception("Empty parameter 'page' in Model");
		}

		$modelName = '\\Models\\Pages\\' . ucfirst($this->page);

		if (class_exists($modelName)) {
			$modelData = new $modelName();
		} else {
			$this->page = 'Error404';
			$modelData = new Pages\Error404();
		}

		return [$this->page => $modelData->getResult()];
	}

	/**
	 * Обработка данных формы
	 *
	 * @param Entity $entity
	 * @return array[]
	 * @throws \Exception
	 */
	private function processingFormData(Entity $entity): array
	{
		if (empty($this->entityName) || empty ($this->action)) {
			throw new \Exception('Empty form handler parameters in Model');
		}

		$inputData = [];
		$entityStructure = EntityStructure::get($entity);

		foreach ($entityStructure as $entityItem) {
			foreach ($entityItem->getFields() as $field) {
				if (isset($this->controller->getRequest()->{$field['Field']}) &&
					!empty($this->controller->getRequest()->{$field['Field']})) {
					$inputData[$field['Field']] = $this->controller->getRequest()->{$field['Field']};
				}
				if (strtolower($field['Type']) === 'blob') {
					$inputData[$field['Field']] = $this->controller->getRequest()->files()->all()[$field['Field']];
				}
			}
		}

		$modelName = '\\Models\\Handlers\\' . ucfirst($this->action);
		$modelData = new $modelName($this->entityName, $inputData);

		return ['handler' => ['result' => $modelData->getResult()]];
	}
}