<?php

namespace System\Core;

use \Klein\Klein as Router,
	Klein\Request,
	System\Helpers\Auth;

/**
 * Контроллер входящих/исходящих данных
 * @package System\Core
 */
class Controller
{
	/**
	 * @var Router Маршрутизатор
	 */
	private Router $router;

	/**
	 * @var Request Входящий запрос
	 */
	private Request $request;

	/**
	 * @var string Метод запроса
	 */
	private string $method;

	/**
	 * @var string Запрашиваемая страница
	 */
	private ?string $page;

	/**
	 * @var string Действие, необходимое для операции с сущностью
	 */
	private string $action;

	/**
	 * @var string Наименование сущности
	 */
	private string $entityName;

	/**
	 * @var string Адрес для редиректа
	 */
	private string $urlRedirect;

	/**
	 * Controller constructor
	 */
	public function __construct()
	{
		$this->router = new Router();
		$this->page = '';

		$this->router->respond('GET', '/[:page]', function (Request $request) {
			$this->method = 'GET';
			$this->request = $request;
			$this->page = $request->page;
		});

		$this->router->respond('POST', '/[:entity]/[:action]', function ($request, $response) {
			$this->method = 'POST';
			$this->request = $request;
			$this->entityName = $request->entity;
			$this->action = $request->action;
		});

		$this->router->dispatch();
	}


	/**
	 * Назначить страницу для редиректа, в зависимости от параметров авторизации пользователя
	 *
	 * @param Auth $auth
	 */
	public function setPageRedirect(Auth $auth)
	{
		if ($auth->isLogin() === false) {
			switch ($this->page) {
				case '' :
				case 'profile' :
				case 'logout' :
					$this->redirectTo('/auth');
					break;
			}
		} else {
			switch ($this->page) {
				case '' :
				case 'auth' :
				case 'reg' :
					$this->redirectTo('/profile');
					break;

				case 'logout' :
					$auth->logout();
					$this->redirectTo('/auth');
					break;
			}
		}
	}

	/**
	 * Получить входящий запрос
	 *
	 * @return Request
	 */
	public function getRequest(): Request
	{
		return $this->request;
	}

	/**
	 * Получить метод запроса
	 *
	 * @return string
	 */
	public function getMethod(): string
	{
		return $this->method;
	}

	/**
	 * Получить наименование страницы запроса
	 *
	 * @return string|null
	 */
	public function getPage(): ?string
	{
		return $this->page;
	}

	/**
	 * Получить наименование текущей сущности для обработки запроса
	 *
	 * @return string
	 */
	public function getEntityName(): string
	{
		return $this->entityName;
	}

	/**
	 * Получить наименование действия с текущей сущностью
	 *
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	/**
	 * Инициировать редирект на определенный url
	 *
	 * @param string $url
	 */
	public function redirectTo(string $url = '/')
	{
		$this->urlRedirect = $url;
		$this->page = trim($url, '/');
		$this->router->respond('GET', '!@^' . $url, function ($request, $response) {
			$response->redirect($this->urlRedirect)->send();
		});

		$this->router->dispatch();
	}
}