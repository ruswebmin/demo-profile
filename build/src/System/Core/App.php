<?php

namespace System\Core;

use System\Helpers\Env;
use System\Helpers\Lang,
	System\Helpers\Config,
	System\Helpers\DB;

/**
 * Сборка приложения
 * @package System\Core
 */
class App
{
	/**
	 * @var View Представление
	 */
	private View $view;

	/**
	 * App constructor
	 * @throws \Exception
	 */
	public function __construct()
	{
		$config = new Config();
		Env::setSystemVar('config', $config->get());

		$lang = new Lang();
		Env::setSystemVar('lang', $lang->getSelectedLang());

		$db = new DB();
		$dbConn = $db->connect();
		Env::setSystemVar('dbConn', $dbConn);

		$controller = new Controller;
		$model = new Model($controller);
		$this->view = new View($model->getData($lang));

		$db->disconnect();
	}

	/**
	 * Получить html-представление
	 *
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function run()
	{
		return $this->view->render();
	}
}