<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
      0 => '9999999-dev',
    ),
    'reference' => '0ed07b27db0197aad3dd89f8410a4f3bdd031abc',
    'name' => 'san/test',
  ),
  'versions' => 
  array (
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.6.2',
      'version' => '4.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '51ac4500c4dc30cbaaabcd2f25694299df666a31',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.1.6',
      'version' => '4.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '5515b6a6c6f1e1c909aaff2e5f3a15c177dfd1a9',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acd0dc8b394595a74b58dcc889f72569ff7d8e71',
    ),
    'codeception/lib-innerbrowser' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2123542b1325cc349ac68868abe74638bcb32ab6',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '79f13d05b63f2fceba4d0e78044bab668c9b2a6b',
    ),
    'codeception/module-phpbrowser' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fbf585c8562e4e4875f351f5392bcb2b1a633cbe',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '9.0.2',
      'version' => '9.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb27243d8edde68593bf8d9ef5e9074734777931',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '3.6.1',
      'version' => '3.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a3ba01414cbee76a1bced9f9b6b169cc8d203880',
    ),
    'delight-im/auth' => 
    array (
      'pretty_version' => 'v8.2.1',
      'version' => '8.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '157a7095b0a4e12246923449e6083212438fc071',
    ),
    'delight-im/base64' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '687b2a49f663e162030a8d27b32838bbe7f91c78',
    ),
    'delight-im/cookie' => 
    array (
      'pretty_version' => 'v3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '67065d34272377d63bab0bd58f984f9b228c803f',
    ),
    'delight-im/db' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7d6f4c7a7e8ba6a297bfc30d65702479fd0b5f7c',
    ),
    'delight-im/http' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a19a72a7eac8b1301aa972fb20cff494ac43e09',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f350df0268e904597e3bd9c4685c53e0e333feea',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'klein/klein' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6549676cc831b9417332b3d9485c64bbf7bac728',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '969b211f9a51aa1f6c01d1d2aef56d3bd91598e5',
      'replaced' => 
      array (
        0 => '1.10.1',
      ),
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.1.0',
      'version' => '5.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd72d394ca794d3466a3b2fc09d5a6c1dc86b47e',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e878a14a65245fbe78f8080eba03b47c3b705651',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => 'v1.10.3',
      'version' => '1.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '451c3cd1418cf640de218914901e51b064abb093',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '8.0.2',
      'version' => '8.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6647ffddd2add025ab3f21644a441d7c146cdc',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e282e5f5e2db5fb2271b3962ad69875c34a6f41',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6eedfed1085dd1f4c599629459a0277d25f9a66',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ff9c8ea4d3212b88fcf74e25e516e2c51c99324',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.1',
      'version' => '5.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc49734779cbb302bf51a44297dab8c4bbf941e7',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '4.0.3',
      'version' => '4.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5672711b6b07b14d5ab694e700c62eeb82fcf374',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.2.5',
      'version' => '9.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad7cc5ec3ab2597b329880e30442d9054526023b',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'san/test' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '0ed07b27db0197aad3dd89f8410a4f3bdd031abc',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1e2df332c905079980b119c4db103117e5e5c90',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee51f9bb0c6d8a43337055db3120829fa14da819',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '4.0.3',
      'version' => '4.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dcc580eadfaa4e7f9d2cf9ae1922134ea962e14f',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e90b4cf905a7d06c420b1d2e9d11a4dc8a13113',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a757cab9d5b7ef49a619f1143e6c9c1bc0fe9d2',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '571d721db4aec847a0e59690b954af33ebf9f023',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bdb1e7c79e592b8c82cb1699be3c8743119b8a72',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '074fed2d0a6d08e1677dd8ce9d32aecb384917b8',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '127a46f6b057441b201253526f81d5406d6c7840',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '062231bf61d2b9448c4fa5a7643b5e1829c11d63',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0653718a5a629b065e91f774595267f8dc32e213',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56b3ba194e0cbaaf3de7ccd353c289d7a84ed022',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '626586115d0ed31cb71483be55beb759b5af5a3c',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9adef763c4f98660d1f8b924f6d61718f8ae0bc',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '34ac555a3627e324b660e318daa07572e1140123',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e544e24472d4c97b2d11ade7caacd446727c6bf9',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd99cb3a0aff6cadd2a8d7d7ed72c2161e218337',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '907187782c465a564f9030a0c6ace59e8821106f',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc0d059e2e997e79ca34125a52f3e33de4424ac7',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '405952c4e90941a17e52ef7489a2bd94870bb290',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4298870062bfc667cb78d2b379be4bf5dec5f187',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2edd75b8b35d62fd3eeabba73b26b8f1f60ce13d',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e4dbcf5e81eba86e36731f94fe56b1726835846',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a57f8161502549a742a63c09f0a604997bf47027',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '40309d1700e8f72447bb9e7b54af756eeea35620',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7110338d81ce1cbc3e273136e4574663627037a7',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f048e612a3905f34931127360bdd2def19a5e582',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0837fe02d617d31fbb25f990655861bb27bd1a',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a5b6bba3259902e386eb80dd1956181ee90b5b2',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '66a8f0957a3ca54e4f724e49028ab19d75a8918b',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac70459db781108db7c6d8981dd31ce0e29e3298',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea342353a3ef4f453809acc4ebc55382231d4d23',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '11336f6f84e16a720dae9d8e6ed5019efa85a0f9',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b88ccd180a6b61ebb517aea3b1a8906762a1dc2',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9dc4f203e36f2b486149058bade43c851dd97451',
    ),
  ),
);
