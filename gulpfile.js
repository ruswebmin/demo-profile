const pathFullStackBuild = 'build/'
const pathDelpoy = '/mnt/d/OpenServer/domains/profile'

const pathFrontend = 'frontend/';
const pathFrontendSrc = pathFrontend + 'src/';
const pathFrontendBuild = pathFrontend + 'build/';

const pathBackend = 'backend/';
const pathBackendSrc = pathBackend + 'src/';

const gulp = require('gulp');
const { src, dest, series, parallel } = require('gulp');
const del= require('del');
const sass = require('gulp-sass');
const fileInclude = require('gulp-file-include');
const concat = require('gulp-concat');
const inject = require('gulp-inject');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const gulpif = require('gulp-if');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const minify = require('gulp-minifier');
const newer = require('gulp-newer');
const changed = require('gulp-changed');

function cleanCssCompile() {
    return del(pathFrontendSrc + 'scss/css/**', {force:true});
}

function cleanFrontendBuild() {
    return del(pathFrontendBuild + '**', {force:true});
}

function cleanFullStackBuild() {
    return del(pathFullStackBuild + '**', {force:true});
}

function cleanDeploy() {
    return del(pathDelpoy + '**', {force:true});
}

function sassToCss() {
    return src(pathFrontendSrc + 'scss/**/*.scss')
        .pipe(changed(pathFrontendSrc + 'scss/css/'))
        .pipe(sass())
        .pipe(dest(pathFrontendSrc + 'scss/css/'));
}

function cssToFrontendBuild() {
    return src(pathFrontendSrc + 'scss/css/**/*.css')
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(concat('all.min.css'))
        .pipe(sourcemaps.write())
        .pipe(dest(pathFrontendBuild + 'assets//css/'));
}

function jsToFrontendBuild() {
    return src(pathFrontendSrc + 'js/**/*.js')
        .pipe(changed(pathFrontendBuild + 'js/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minify({
            minify: true,
            minifyJS: {
                sourceMap: true
            }
        }))
        .pipe(dest(pathFrontendBuild + 'assets/js/'));
}

function htmlToFrontendBuild() {
    return src(pathFrontendSrc + 'html/**/*.html')
        .pipe(changed(pathFrontendBuild))
        .pipe(dest(pathFrontendBuild));
}

function assetsInjectToHeader() {
    return src(pathFrontendSrc + 'html/components/header.html')
        .pipe(inject(src([
                pathFrontendBuild + 'assets/**/*.css',
                pathFrontendBuild + 'assets/**/*.js'],
            {read: false}),
            {relative: true}))
        .pipe(replace('../../build/assets/', ''))
        .pipe(dest(pathFrontendBuild + 'components/'));
}

function htmlCompile() {
    return src([
        pathFrontendSrc + 'html/*.html',
        '!**/header.html',
        '!**/footer.html'
    ])
        // .pipe(changed(pathFrontendBuild))
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest(pathFrontendBuild));
}

function assetsCssCopy() {
    return src([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/bootstrap/dist/css/bootstrap.min.css.map',
    ])
        .pipe(dest(pathFrontendBuild + 'assets/css'))
}

function assetsJsCopy() {
    return src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery/dist/jquery.min.map',
        'node_modules/jquery.cookie/jquery.cookie.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js.map',
    ])
        .pipe(gulpif(isJQuery, rename({
            prefix: '_'
        })))
        .pipe(dest(pathFrontendBuild + 'assets/js'))
}

function isJQuery(file) {
    return file.basename === 'jquery.min.js';
}

function backendCopyToFullStackBuild() {
    return src([
        pathBackend + '**/*.php',
        pathBackend + '**/*.ini',
        pathBackend + '**/*.yml',
        pathBackend + '**/*.sql',
        pathBackend + '**/.htaccess',
        '!**/composer.*',
        // pathFrontend + '**/*.html'
    ])
        .pipe(changed(pathFullStackBuild))
        .pipe(dest(pathFullStackBuild));
}

function assetsCopyToFullStackBuild() {
    return src([
        pathFrontendBuild + 'assets/**',
        '!**/*.html'
    ])
        .pipe(dest(pathFullStackBuild))
}

function htmlCopyToFullStackBuild() {
    return src([
        pathFrontendSrc + 'html/**/*.html',
        pathFrontendBuild + '**/components/header.html',
    ])
        // .pipe(changed(pathFullStackBuild + 'src/Views'))
        .pipe(replace(new RegExp('@@include.+/(.+)\'.+', 'g'), '{{ include(\'components/$1\') }}'))
        .pipe(dest(pathFullStackBuild + 'src/Views'))
}

function deployBuild() {
    return src([pathFullStackBuild + '**'], { dot: true })
        .pipe(changed(pathDelpoy))
        .pipe(dest(pathDelpoy));
}

/**
 * Helper tasks
 */
exports.cleancss = cleanCssCompile
exports.cleanfront = cleanFrontendBuild
exports.cleanfull = cleanFullStackBuild
exports.cleandeploy = cleanDeploy
exports.clean = parallel(cleanCssCompile, cleanFrontendBuild, cleanFullStackBuild, cleanDeploy)
exports.csscompile = sassToCss
exports.cssbuild = cssToFrontendBuild
exports.jsbuild = jsToFrontendBuild
exports.htmlbuild = htmlToFrontendBuild
exports.assetscopy = series(assetsCssCopy, assetsJsCopy)
exports.assetsinject = assetsInjectToHeader
exports.htmlcompile = htmlCompile
exports.build = series(backendCopyToFullStackBuild, assetsCopyToFullStackBuild, htmlCopyToFullStackBuild)
exports.deploy = deployBuild

/**
 * Main tasks
 */
exports.make = series(
    sassToCss,
    cssToFrontendBuild,
    jsToFrontendBuild,
    htmlToFrontendBuild,
    parallel(assetsCssCopy, assetsJsCopy),
    assetsInjectToHeader,
    htmlCompile,
    series(backendCopyToFullStackBuild, assetsCopyToFullStackBuild, htmlCopyToFullStackBuild),
    deployBuild
)

/**
 * Watchers
 */
gulp.task('watch', function(){
    gulp.watch([
        pathFrontendSrc + 'scss/**/*.scss',
        pathFrontendSrc + 'js/**/*.js',
    ], series(
        sassToCss,
        cssToFrontendBuild,
        jsToFrontendBuild,
        assetsCopyToFullStackBuild,
        deployBuild
    ));

    gulp.watch([
        pathFrontendSrc + '**/*.html',
    ], series(
        htmlCompile,
        htmlCopyToFullStackBuild,
        deployBuild
    ));

    gulp.watch([
        pathBackendSrc + '**/*.*',
        pathBackend + '.htaccess',
        pathBackend + 'index.php',
    ], series(
        backendCopyToFullStackBuild,
        deployBuild
    ));
});